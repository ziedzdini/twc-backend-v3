package com.FiveSkills.BackEndV01.participantNonConfirme.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.FiveSkills.BackEndV01.participantNonConfirme.model.ParticipantNonConfirme;
import com.FiveSkills.BackEndV01.participantNonConfirme.service.ParticipantNonConfirmeService;
import com.FiveSkills.BackEndV01.participantconfirme.model.ParticipantConfirme;
import com.FiveSkills.BackEndV01.participantconfirme.service.ParticipantConfirmeService;

@CrossOrigin
@RestController
@RequestMapping(value="/gestionParticipantNonConfirme")
public class ParticipantNonConfirmeController {
	
	@Autowired 
	ParticipantNonConfirmeService participantService ;
	@Autowired 
	ParticipantConfirmeService participantConfirmeService ;
	
	//add new participants
	@PostMapping(value="/addParticipantNonConfirme")
	public ParticipantNonConfirme addParticipantNonConfirme(@Valid @RequestBody ParticipantNonConfirme participant) {
        LocalDate localDate = LocalDate.now();
        String  datecreation= DateTimeFormatter.ofPattern("yyy-MM-dd").format(localDate);
        participant.setDatederegistre(datecreation);
		return participantService.save(participant) ;
	}
	
	//get all participants 
	
	@GetMapping(value="/listParticipantNonConfirme")
	public List<ParticipantNonConfirme> listParticipantNonConfirme(){
		return participantService.findAll();
	}
	
	//get all participants non contact (dejacontacte=false)
	
	@GetMapping(value="/listParticipantNonConfirmeNonContacte")
	public List<ParticipantNonConfirme> listParticipantNonConfirmeNonContacte(){
		String contactOuNon ="false" ;
		return participantService.findByDejaContacte(contactOuNon) ;
	}
	
	//get the number of participant non contact
	
	@GetMapping(value="/numberOfParticipantNonConfirmeNonContacte")
	public Long numberOfParticipantNonConfirmeNonContacte() {
		String contactOuNon ="false" ;
		List<ParticipantNonConfirme> listParticipantNonconfirme= participantService.findByDejaContacte(contactOuNon) ;
		return (long) listParticipantNonconfirme.size();
	}

	//mark as deja contacte
	
	@PutMapping(value="/markParticipantNonConfirmeAsDejaContacte/{id}")
	public ResponseEntity<ParticipantNonConfirme> markParticipantNonConfirmeAsContacte(@PathVariable(value="id")Long participantid){
		ParticipantNonConfirme participant = participantService.findOne(participantid);
		if(participant==null) {
			return ResponseEntity.notFound().build() ;
		}
		participant.setDejacontacte("true");
		ParticipantNonConfirme participantUpdated=participantService.save(participant) ;
		return ResponseEntity.ok().body(participantUpdated) ;
	}
	
	//confirme paritcipant 
	
	@PutMapping(value="/confirmeParticipant/{id}")
	public ResponseEntity<ParticipantNonConfirme> confirmeParticipant(@PathVariable(value="id") Long participantid ){
		ParticipantNonConfirme participant = participantService.findOne(participantid) ;
		if (participant==null) {
			return ResponseEntity.notFound().build() ;
		}
		participant.setDejaconfirme("true");
		participant.setDejacontacte("true");
		ParticipantNonConfirme participantConfirmed =participantService.save(participant) ;
		return ResponseEntity.ok().body(participantConfirmed) ;

		
	}
	
	//get one participant 
	
	@GetMapping(value="/getParticipantNonConfirme/{id}")
	public ResponseEntity<ParticipantNonConfirme> getParticipantNonConfirme(@PathVariable(value="id") Long participantid){
		ParticipantNonConfirme participant=participantService.findOne(participantid) ;
		if(participant==null) {
			return ResponseEntity.notFound().build() ;
		}
		return ResponseEntity.ok().body(participant) ;
	}
	
	//delete participant 
	
	@DeleteMapping(value="/deleteParticipantNonConfirme/{id}")
	public ResponseEntity<ParticipantNonConfirme> deleteParticipantNonConfirme(@PathVariable(value="id") Long participantid){
		ParticipantNonConfirme participant = participantService.findOne(participantid) ;
		if(participant==null) {
			return ResponseEntity.notFound().build() ;
		}
		participantService.delete(participant);
		return ResponseEntity.ok().body(participant) ;
	}
}
