package com.FiveSkills.BackEndV01.participantNonConfirme.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FiveSkills.BackEndV01.participantNonConfirme.model.ParticipantNonConfirme;
import com.FiveSkills.BackEndV01.participantNonConfirme.repository.ParticipantNonConfirmeRepository;

@Service
public class ParticipantNonConfirmeService {

	@Autowired
	ParticipantNonConfirmeRepository participantRepository ;
	
	//save a participant 
	
	public ParticipantNonConfirme save(ParticipantNonConfirme participant) {
		return participantRepository.save(participant) ;
	}
	
	//get all participants
	
	public List<ParticipantNonConfirme> findAll(){
		return participantRepository.findAll();
	}
	
	//get one participant
	
	public ParticipantNonConfirme findOne(Long participantid) {
		return participantRepository.findById(participantid).get();
	}
	
	//delete participant 
	
	public void delete(ParticipantNonConfirme participantid) {
		participantRepository.delete(participantid);
	}
	
	//get participant by dejacontace
	
	public List<ParticipantNonConfirme> findByDejaContacte(String contacteOUnon){
		return participantRepository.findByDejacontacte(contacteOUnon);
	}
}
