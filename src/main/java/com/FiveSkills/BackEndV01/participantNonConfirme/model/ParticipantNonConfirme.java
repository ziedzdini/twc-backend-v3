package com.FiveSkills.BackEndV01.participantNonConfirme.model;

import javax.persistence.*;

@Entity
@Table(name="participantnonconfirme")
public class ParticipantNonConfirme {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	
	private String nom ;
	
	private String prenom ;
	
	private String mail ;
	
	private String telephone ;
	
	private String profession ;
	
	private String lieudetravail ;
	
	private String dejacontacte ;
	
	private String dejaconfirme ;
	
	private String datederegistre ;
	
	private String formation ;
	
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getLieudetravail() {
		return lieudetravail;
	}

	public void setLieudetravail(String lieudetravail) {
		this.lieudetravail = lieudetravail;
	}

	public String getDejacontacte() {
		return dejacontacte;
	}

	public void setDejacontacte(String dejacontacte) {
		this.dejacontacte = dejacontacte;
	}

	public String getDejaconfirme() {
		return dejaconfirme;
	}

	public void setDejaconfirme(String dejaconfirme) {
		this.dejaconfirme = dejaconfirme;
	}

	public String getDatederegistre() {
		return datederegistre;
	}

	public void setDatederegistre(String datederegistre) {
		this.datederegistre = datederegistre;
	}

	public String getFormation() {
		return formation;
	}

	public void setFormation(String formation) {
		this.formation = formation;
	}
	
	
}
