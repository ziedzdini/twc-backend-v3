package com.FiveSkills.BackEndV01.participantNonConfirme.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FiveSkills.BackEndV01.participantNonConfirme.model.ParticipantNonConfirme;

public interface ParticipantNonConfirmeRepository extends JpaRepository<ParticipantNonConfirme,Long> {

	List<ParticipantNonConfirme> findByDejacontacte(String contacteOUnon) ;
}
