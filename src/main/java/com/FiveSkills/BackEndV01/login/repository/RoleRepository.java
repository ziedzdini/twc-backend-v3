package com.FiveSkills.BackEndV01.login.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.FiveSkills.BackEndV01.login.model.ERole;
import com.FiveSkills.BackEndV01.login.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}