package com.FiveSkills.BackEndV01.login.model;

public enum ERole {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}