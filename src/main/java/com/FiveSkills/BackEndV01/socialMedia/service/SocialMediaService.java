package com.FiveSkills.BackEndV01.socialMedia.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FiveSkills.BackEndV01.socialMedia.model.SocialMedia;
import com.FiveSkills.BackEndV01.socialMedia.repository.SocialMediaRepository;

@Service
public class SocialMediaService {
	
	@Autowired
	SocialMediaRepository socialMediaRepository ;
	
	//add socialmedia link 
	
	public SocialMedia save(SocialMedia socialmedia) {
		return socialMediaRepository.save(socialmedia) ;
	}
	
	//get all socialmedia 
	
	public List<SocialMedia> findAll(){
		return socialMediaRepository.findAll() ;
	}
	
	//get one link 
	
	public SocialMedia findOne(Long socialmediaid) {
		return socialMediaRepository.findById(socialmediaid).get() ;
	}
	
	//get link by nom 
	
	public SocialMedia findByNom(String nom) {
		return socialMediaRepository.findByNom(nom) ;
	}
	
	//delete link 
	
	public void delete(SocialMedia socialmedia) {
		socialMediaRepository.delete(socialmedia);
	}

}
