package com.FiveSkills.BackEndV01.socialMedia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FiveSkills.BackEndV01.socialMedia.model.SocialMedia;

public interface SocialMediaRepository extends JpaRepository<SocialMedia,Long> {

	SocialMedia findByNom(String nom) ;
}
