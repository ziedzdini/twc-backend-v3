package com.FiveSkills.BackEndV01.socialMedia.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.FiveSkills.BackEndV01.socialMedia.model.SocialMedia;
import com.FiveSkills.BackEndV01.socialMedia.service.SocialMediaService;

@CrossOrigin
@RestController
@RequestMapping(value="/socialMedia")
public class SocialMediaController {
	
	@Autowired 
	SocialMediaService socialMediaService ;
	
	//add socialMedia url
	@PostMapping(value="/addSocialMediaUrl")
	public SocialMedia addSocialMediaUrl(@Valid @RequestBody SocialMedia socialmedia) {
		return socialMediaService.save(socialmedia) ;
	}
	
	//delete socialMedia url 
	@DeleteMapping(value="/deleteSocialMedia/{id}")
	public ResponseEntity<SocialMedia> deleteSocialMedia(@PathVariable(value="id") Long socialMediaID) {
		SocialMedia socialmedia = socialMediaService.findOne(socialMediaID) ;
		if(socialmedia==null) {
			return ResponseEntity.notFound().build() ;
		}
		socialMediaService.delete(socialmedia);
		return ResponseEntity.ok().body(socialmedia) ;
	}
	
	
	
	
	//update socialMedia  url 
	@PutMapping(value="/changeSocialMediaUrl/{nom}")
	public ResponseEntity<SocialMedia> changeSocialMediaUrl(@PathVariable(value="nom")String nom , @RequestBody String socialmediaUrl){
		SocialMedia socialMedia = socialMediaService.findByNom(nom) ;
		if(socialMedia==null) {
			return ResponseEntity.notFound().build() ;
		}
		socialMedia.setUrl(socialmediaUrl);
		
		SocialMedia socialMediaUpdated = socialMediaService.save(socialMedia) ;
		
		return ResponseEntity.ok().body(socialMediaUpdated) ;
	}
	
	
	//get socialMedia by nom 
	
	@GetMapping(value="/socialMediaByNom/{nom}")
	public ResponseEntity<SocialMedia> getSocialMediaByNom(@PathVariable(value="nom") String nom){
		SocialMedia socialmedia = socialMediaService.findByNom(nom) ;
		if(socialmedia==null) {
			return ResponseEntity.notFound().build() ;
		}
		return ResponseEntity.ok().body(socialmedia) ;
	}
	
	

}
