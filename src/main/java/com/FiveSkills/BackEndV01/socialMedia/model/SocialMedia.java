package com.FiveSkills.BackEndV01.socialMedia.model;

import javax.persistence.*;

@Entity
@Table(name="socialmedia")
public class SocialMedia {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	
	private String nom ;
	
	private String url ;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	

}
