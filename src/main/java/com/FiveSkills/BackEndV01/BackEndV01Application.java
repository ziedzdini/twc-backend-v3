package com.FiveSkills.BackEndV01;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.FiveSkills.BackEndV01.login.model.ERole;
import com.FiveSkills.BackEndV01.login.model.Role;
import com.FiveSkills.BackEndV01.login.model.User;
import com.FiveSkills.BackEndV01.login.repository.RoleRepository;
import com.FiveSkills.BackEndV01.login.repository.UserRepository;
import com.FiveSkills.BackEndV01.payload.response.MessageResponse;

@SpringBootApplication
public class BackEndV01Application implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BackEndV01Application.class, args);
	}

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Override
	public void run(String... args) throws Exception {

		if (roleRepository.findAll().size() == 0) {
			Role r1 = new Role();
			r1.setId(1);
			r1.setName(ERole.ROLE_USER);
			roleRepository.save(r1);

			Role r2 = new Role();
			r2.setId(2);
			r2.setName(ERole.ROLE_MODERATOR);
			roleRepository.save(r2);

			Role r3 = new Role();
			r3.setId(3);
			r3.setName(ERole.ROLE_ADMIN);
			roleRepository.save(r3);

		}

		if (userRepository.existsByUsername("ziedzdini")) {

		} else {

			// Create new user's account
			User user = new User("ziedzdini", "zied.zdini@espit.tn", encoder.encode("ziedzdini"));

			Set<Role> roles = new HashSet<>();

			Role userRole = roleRepository.findByName(ERole.ROLE_USER).get();
			if (userRole != null)
				roles.add(userRole);

			Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN).get();
			if (adminRole != null)
				roles.add(adminRole);

			Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR).get();
			if (modRole != null)
				roles.add(modRole);

			user.setRoles(roles);
			userRepository.save(user);
		}
	}

}
