package com.FiveSkills.BackEndV01.planing.model;

import javax.persistence.*;

@Entity
@Table(name="planings")
public class PlaningDeFormation {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	
	private Long formationid ;
	
	private String planing ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFormationid() {
		return formationid;
	}

	public void setFormationid(Long formationid) {
		this.formationid = formationid;
	}

	public String getPlaning() {
		return planing;
	}

	public void setPlaning(String planing) {
		this.planing = planing;
	}

}
