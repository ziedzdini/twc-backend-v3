package com.FiveSkills.BackEndV01.planing.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FiveSkills.BackEndV01.planing.model.PlaningDeFormation;

public interface PlaningDeFormationRepository extends JpaRepository<PlaningDeFormation,Long> {

	List<PlaningDeFormation>findByFormationid(Long formationid) ;
}
