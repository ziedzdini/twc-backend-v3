package com.FiveSkills.BackEndV01.planing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FiveSkills.BackEndV01.planing.model.PlaningDeFormation;
import com.FiveSkills.BackEndV01.planing.repository.PlaningDeFormationRepository;


@Service
public class PlaningDeFormationService {
	
	@Autowired
	PlaningDeFormationRepository planingRepository ;
	
	//save an planing
	
	public PlaningDeFormation save(PlaningDeFormation objectif) {
		return planingRepository.save(objectif) ;
	}
	
	//get all planing
	
	public List<PlaningDeFormation> findAll() {
		return planingRepository.findAll() ;
	}
	
	//get one planing
	
	public PlaningDeFormation findOne(Long objectifId) {
		return planingRepository.findById(objectifId).get() ;
	}
	
	public void delete(PlaningDeFormation objectif) {
		planingRepository.delete(objectif);
	}
	
	//get  planing by formationid
	
	public List<PlaningDeFormation> findbyFormationid(Long formationid){
		return planingRepository.findByFormationid(formationid) ;
	}


}
