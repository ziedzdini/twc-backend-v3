package com.FiveSkills.BackEndV01.planing.controller;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.FiveSkills.BackEndV01.planing.model.PlaningDeFormation;
import com.FiveSkills.BackEndV01.planing.service.PlaningDeFormationService;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value="/gestionPlaning")
public class PlaningDeFormationController {

	
	@Autowired 
	PlaningDeFormationService planingService ;
	
	//add planing
	
	@PostMapping(value="/addPlaning")
	public PlaningDeFormation addPlaning(@Valid @RequestBody PlaningDeFormation planing) {
		return planingService.save(planing) ;
	}
	
	//get all planing 
	
	@GetMapping(value="/listPlaning")
	public List<PlaningDeFormation> listPlaning(){
		
		return planingService.findAll() ;
	}
	
	
	//delete planing 
	
	@DeleteMapping(value="/deletePlaning/{id}")
	public ResponseEntity<PlaningDeFormation> deletePlaning(@PathVariable(value="id") Long planingid){
		PlaningDeFormation planing=planingService.findOne(planingid) ;
		if(planing==null) {
			return ResponseEntity.notFound().build() ;
		}
		planingService.delete(planing);
		return ResponseEntity.ok().body(planing) ;
	}
	
	//update planing 
	
	@PutMapping(value="/updatePlaning/{id}")
	public ResponseEntity<PlaningDeFormation> updatePlaning(@PathVariable(value="id") Long planingid ,@Valid @RequestBody PlaningDeFormation planing ){
		PlaningDeFormation planig=planingService.findOne(planingid) ;
		if(planig==null) {
			return ResponseEntity.notFound().build() ;
		}
		planig.setPlaning(planing.getPlaning());
		planig.setFormationid(planing.getFormationid());
	 	PlaningDeFormation planingToSave= planingService.save(planig);
	 	return ResponseEntity.ok().body(planingToSave) ;
	}
	
	//get planing by formationid
	
	@GetMapping(value="/getPlaningByFormationId/{formationid}")
	public List<PlaningDeFormation> getPlaningByFormationId(@PathVariable(value="formationid") Long formationid){
		return planingService.findbyFormationid(formationid) ;
	}
	
	
}
