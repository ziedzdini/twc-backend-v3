package com.FiveSkills.BackEndV01.session.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FiveSkills.BackEndV01.session.model.Session;

public interface SessionRepository extends JpaRepository<Session,Long> {

	
}
