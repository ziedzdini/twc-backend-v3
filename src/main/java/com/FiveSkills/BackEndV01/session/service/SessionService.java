package com.FiveSkills.BackEndV01.session.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FiveSkills.BackEndV01.session.model.Session;
import com.FiveSkills.BackEndV01.session.repository.SessionRepository;

@Service
public class SessionService {

	@Autowired
	SessionRepository sessionRepository ;
	
	//add session
	
	public Session save(Session session) {
		return sessionRepository.save(session) ;
	}
	
	//get all session
	
	public List<Session> findAll(){
		return sessionRepository.findAll() ;
	}
	
	//get session byId
	
	public Session findOne(Long sessionid) {
		return sessionRepository.findById(sessionid).get() ;
	}
	
	//delete session
	
	public void delete(Session session) {
		sessionRepository.delete(session);
	}
}
