package com.FiveSkills.BackEndV01.session.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.FiveSkills.BackEndV01.session.model.Session;
import com.FiveSkills.BackEndV01.session.service.SessionService;

@CrossOrigin
@RestController
@RequestMapping(value = "/gestionSession")
public class SessionController {

    @Autowired
    SessionService sessionService;

    //save session
    @PostMapping(value = "/addSession")
    public Session addSession(@Valid @RequestBody Session session) {
        LocalDate localDate = LocalDate.now();
        String datecreation = DateTimeFormatter.ofPattern("yyy-MM-dd").format(localDate);
        session.setDatecreation(datecreation);
        Long a = (long) 0;
        String dateDebut = session.getDatedebut().substring(0, 10);
        String dateFin = session.getDatefin().substring(0, 10);
        session.setDatedebut(dateDebut);
        session.setDatefin(dateFin);
        session.setNombreexistent(a);
        return sessionService.save(session);
    }

    //get all session
    @GetMapping(value = "/listSession")
    public List<Session> getListSession() {
        return sessionService.findAll();
    }

    //get one session
    @GetMapping(value = "/getSession/{id}")
    public ResponseEntity<Session> getSession(@PathVariable(value = "id") Long sessionid) {

        Session session = sessionService.findOne(sessionid);
        if (session == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(session);
    }

    //update session
    @PutMapping(value = "/updateSession/{id}")
    public ResponseEntity<Session> updateSession(@PathVariable(value = "id") Long sessionid, @Valid @RequestBody Session session) {
        Session sessionInDB = sessionService.findOne(sessionid);

        if (sessionInDB == null) {
            return ResponseEntity.notFound().build();
        }

        if (session.getDatedebut() != null) {
            String dateDebut = session.getDatedebut().substring(0, 10);
            sessionInDB.setDatedebut(dateDebut);
        }

        if (session.getDatefin() != null) {
            String dateFin = session.getDatefin().substring(0, 10);
            sessionInDB.setDatefin(dateFin);
        }

        sessionInDB.setNom(session.getNom());
        sessionInDB.setDatecreation(session.getDatecreation());
        sessionInDB.setFormateurid(session.getFormateurid());
        sessionInDB.setNombreexistent(session.getNombreexistent());
        sessionInDB.setNombreplacelimite(session.getNombreplacelimite());
        sessionInDB.setFormation(session.getFormation());
        session = sessionService.save(sessionInDB);
        return ResponseEntity.ok().body(session);
    }

    //delete session
    @DeleteMapping(value = "/deleteSession/{id}")
    public ResponseEntity<Session> deleteSession(@PathVariable(value = "id") Long sessionid) {
        Session sessionInDB = sessionService.findOne(sessionid);
        if (sessionInDB == null) {
            return ResponseEntity.notFound().build();
        }
        sessionService.delete(sessionInDB);
        return ResponseEntity.ok().body(sessionInDB);
    }

}
