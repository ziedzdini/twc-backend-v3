package com.FiveSkills.BackEndV01.session.model;

import javax.persistence.*;

@Entity
@Table(name="sessions")
public class Session {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	
	private String nom ;
	
	private String formation ;
	
	private String datecreation ;
	
	private String datedebut ;
	
	private String datefin ;
	
	private Long nombreplacelimite ;
	
	private Long nombreexistent ;
	
	private Long formateurid ;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	

	public String getFormation() {
		return formation;
	}

	public void setFormation(String formation) {
		this.formation = formation;
	}

	public String getDatecreation() {
		return datecreation;
	}

	public void setDatecreation(String datecreation) {
		this.datecreation = datecreation;
	}


	public String getDatedebut() {
		return datedebut;
	}

	public void setDatedebut(String datedebut) {
		this.datedebut = datedebut;
	}

	public String getDatefin() {
		return datefin;
	}

	public void setDatefin(String datefin) {
		this.datefin = datefin;
	}

	public Long getNombreplacelimite() {
		return nombreplacelimite;
	}

	public void setNombreplacelimite(Long nombreplacelimite) {
		this.nombreplacelimite = nombreplacelimite;
	}

	public Long getNombreexistent() {
		return nombreexistent;
	}

	public void setNombreexistent(Long nombreexistent) {
		this.nombreexistent = nombreexistent;
	}

	public Long getFormateurid() {
		return formateurid;
	}

	public void setFormateurid(Long formateurid) {
		this.formateurid = formateurid;
	}



}
