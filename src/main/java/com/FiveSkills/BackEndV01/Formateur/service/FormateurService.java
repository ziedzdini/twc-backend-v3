package com.FiveSkills.BackEndV01.Formateur.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FiveSkills.BackEndV01.Formateur.model.Formateur;
import com.FiveSkills.BackEndV01.Formateur.repository.FormateurRepository;

@Service
public class FormateurService {

	@Autowired 
	FormateurRepository formateurRepository ;
	
	
	//save formateur 
	
	public Formateur save(Formateur formateur) {
		
		return formateurRepository.save(formateur) ;
	}
	
	//search all formateurs
	
	public List<Formateur> findAll(){
		return formateurRepository.findAll();
	}
	
	//find one formateur 
	
	public Formateur findOne(Long FormateurId) {
		return formateurRepository.findById(FormateurId).get() ;
	}
	
	//delete formateur 
	
	public void delete(Formateur formateur){
		formateurRepository.delete(formateur);
	}
	
	public List<Formateur> findByStatus(String status){
		return formateurRepository.findByStatus(status);
	}
}
