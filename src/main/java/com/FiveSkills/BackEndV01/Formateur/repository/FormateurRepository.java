package com.FiveSkills.BackEndV01.Formateur.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FiveSkills.BackEndV01.Formateur.model.Formateur;

public interface FormateurRepository extends JpaRepository<Formateur,Long> {

	List<Formateur> findByStatus(String status) ;
	
}
