package com.FiveSkills.BackEndV01.Formateur.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="Formateurs")
@EntityListeners(AuditingEntityListener.class)
public class Formateur {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id ;
	
	private String nom ;
	
	private String prenom ;
	
	private String mail ;
	
	private String telephone ;
	
	private String formation ;
	
	private String status ;
	
	private String file ;
	
	
	
	public String getCv() {
		return file;
	}

	public void setCv(String cvFileName) {
		this.file = cvFileName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFormation() {
		return formation;
	}

	public void setFormation(String formation) {
		this.formation = formation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	
	
}
