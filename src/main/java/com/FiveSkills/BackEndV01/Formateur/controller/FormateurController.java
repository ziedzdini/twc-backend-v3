package com.FiveSkills.BackEndV01.Formateur.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.FiveSkills.BackEndV01.Formateur.model.Formateur;
import com.FiveSkills.BackEndV01.Formateur.service.FormateurService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin
@RestController
@RequestMapping("/gestionFormateur")
public class FormateurController {

    @Autowired
    FormateurService formateurService;
    @Autowired
    ServletContext context;

    @PostMapping(value = "addFormateur")
    public ResponseEntity<String> addFormateur(@RequestParam(name = "cvFormateur", required = false) MultipartFile cv, @RequestParam("formateur") String formateur) throws JsonParseException, JsonMappingException, IOException {

        Formateur formater = new ObjectMapper().readValue(formateur, Formateur.class);

        boolean isExist = new File(context.getRealPath("/formateurCv/")).exists();
        if (!isExist) {
            new File(context.getRealPath("/formateurCv/")).mkdir();
        }
        if (cv != null) {
            long timeToMakeDeff = System.currentTimeMillis();
            String filename = cv.getOriginalFilename();
            String modifiedFileNameToStoreInDB = FilenameUtils.getBaseName(timeToMakeDeff + "_" + filename);
            String modifiedFileName = FilenameUtils.getBaseName(timeToMakeDeff + "_" + filename + "." + FilenameUtils.getExtension(filename));
            File file = new File(context.getRealPath("/formateurCv/" + File.separator + modifiedFileName));
            try {
                FileUtils.writeByteArrayToFile(file, cv.getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
            //
            formater.setCv(modifiedFileNameToStoreInDB);
        }
        formater.setStatus("travaillant actuel");
        Formateur dbFormateur = formateurService.save(formater);
        if (dbFormateur != null) {

            String idFormateur = Long.toString(dbFormateur.getId());
            return new ResponseEntity<String>(idFormateur, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("not done !", HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(value = "listFormateur")
    public List<Formateur> getListFormateur() {

        return formateurService.findAll();
    }

    @RequestMapping(value = "/getcvFormateur", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<InputStreamResource> downloadPDFFile()
            throws IOException {

        ClassPathResource pdfFile = new ClassPathResource("hanicv (2).pdf");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(pdfFile.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(pdfFile.getInputStream()));
    }

    private static final String EXTENSION = ".pdf";
    private static final String SERVER_LOCATION = "D:/FiveSkillsV01/BackEndV01/src/main/webapp/formateurCv";

    @GetMapping(value = "/getcv")
    public ResponseEntity<ByteArrayResource> download(@RequestParam("cv") String cv) throws IOException {

        File file = new File(SERVER_LOCATION + File.separator + cv + EXTENSION);

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + cv + ".pdf");
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    @DeleteMapping(value = "/deleteFormateur/{id}")
    public ResponseEntity<Formateur> deleteForamteur(@PathVariable(value = "id") Long idFormateur) {
        Formateur formateurToDelete = formateurService.findOne(idFormateur);
        if (formateurToDelete == null) {
            return ResponseEntity.notFound().build();
        }
        formateurService.delete(formateurToDelete);
        return ResponseEntity.ok().body(formateurToDelete);
    }

    @PutMapping(value = "/banFormateur/{id}")
    public ResponseEntity<Formateur> banFormateur(@PathVariable(value = "id") Long idFormateur) {
        Formateur formateurToBan = formateurService.findOne(idFormateur);
        if (formateurToBan == null) {
            return ResponseEntity.notFound().build();

        }
        formateurToBan.setStatus("ancien");
        Formateur banFormateur = formateurService.save(formateurToBan);
        return ResponseEntity.ok().body(banFormateur);
    }

    //get one formateur 
    @GetMapping(value = "/getFormateur/{id}")

    public Formateur getOneFormateur(@PathVariable(value = "id") Long idFormateur) {
        Formateur formateurToReturn = formateurService.findOne(idFormateur);

        return formateurToReturn;

    }

    //update a formateur 
    @PutMapping(value = "/updateFormateur/{id}")
    public ResponseEntity<String> getOneFormateur(@PathVariable(value = "id") Long idFormateur, @RequestParam(required = false, value = "cvFormateur") MultipartFile cv, @RequestParam("formateur") String formateur) throws JsonParseException, JsonMappingException, IOException {

        Formateur formateurExistInDB = formateurService.findOne(idFormateur);

        Formateur formateurNewDetails = new ObjectMapper().readValue(formateur, Formateur.class);

        boolean isExist = new File(context.getRealPath("/formateurCv/")).exists();
        if (!isExist) {
            new File(context.getRealPath("/formateurCv/")).mkdir();
        }

        if (cv != null) {
            String filename = cv.getOriginalFilename();
            String modifiedFileNameToStoreInDB = FilenameUtils.getBaseName(System.currentTimeMillis() + "_" + filename);
            String modifiedFileName = FilenameUtils.getBaseName(System.currentTimeMillis() + "_" + filename + "." + FilenameUtils.getExtension(filename));
            File file = new File(context.getRealPath("/formateurCv/" + File.separator + modifiedFileName));
            formateurExistInDB.setCv(modifiedFileNameToStoreInDB);
            try {
                FileUtils.writeByteArrayToFile(file, cv.getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        formateurExistInDB.setStatus(formateurNewDetails.getStatus());
        formateurExistInDB.setNom(formateurNewDetails.getNom());
        formateurExistInDB.setPrenom(formateurNewDetails.getPrenom());
        formateurExistInDB.setMail(formateurNewDetails.getMail());
        formateurExistInDB.setTelephone(formateurNewDetails.getTelephone());
        formateurExistInDB.setFormation(formateurNewDetails.getFormation());

        Formateur dbFormateur = formateurService.save(formateurExistInDB);
        if (dbFormateur != null) {

            return new ResponseEntity<String>("done", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("not done !", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "getFormateurByStatus/{status}")

    public List<Formateur> getFormateurByStatus(@PathVariable(value = "status") String status) {
        return formateurService.findByStatus(status);
    }

}
