package com.FiveSkills.BackEndV01.categorie.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FiveSkills.BackEndV01.categorie.model.Categorie;

public interface CategorieRepository extends JpaRepository<Categorie,Long> {

List<Categorie> findByStatus(String status) ;	

}
