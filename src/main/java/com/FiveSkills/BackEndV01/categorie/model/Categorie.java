package com.FiveSkills.BackEndV01.categorie.model;

import javax.persistence.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="Categories")
@EntityListeners(AuditingEntityListener.class)
public class Categorie {
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	private String nom ;
	private String status ;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
