package com.FiveSkills.BackEndV01.categorie.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FiveSkills.BackEndV01.categorie.model.Categorie;
import com.FiveSkills.BackEndV01.categorie.repository.CategorieRepository;
@Service
public class CategorieService {
	@Autowired 
	CategorieRepository categorieRepository ;

	//save categorie
	
	public Categorie save(Categorie categorie) {
		return  categorieRepository.save(categorie) ;
	}
	
	//get all categories 
	
	public List<Categorie> findAll(){
		return categorieRepository.findAll() ;
	}
	
	//get one categorie 
	
	public Categorie getOne(Long idCategorie) {
		return categorieRepository.findById(idCategorie).get() ;
	}
	
	//delete categorie 
	
	public void deleteOne(Categorie cat) {

		categorieRepository.delete(cat);
	}
	
	
	public List<Categorie> findByStatus(String status){
		return categorieRepository.findByStatus(status);
	}
	
}
