package com.FiveSkills.BackEndV01.categorie.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.FiveSkills.BackEndV01.Formateur.model.Formateur;
import com.FiveSkills.BackEndV01.categorie.model.Categorie;
import com.FiveSkills.BackEndV01.categorie.service.CategorieService;

@CrossOrigin
@RestController
@RequestMapping("/gestionCategorie")
public class CategorieController {

	
	@Autowired
	CategorieService categorieService ;
	
	
	//save categorie
	
	@PostMapping("/addCategorie")
	public Categorie saveCategorie(@Valid @RequestBody Categorie categorie ) {
		return categorieService.save(categorie) ;
	}
	
	//update categorie 
	
	@PutMapping(value="/updateCategorie/{id}")
	public ResponseEntity<Categorie> updateCategorie(@PathVariable(value="id") Long idCategorie ,@Valid @RequestBody Categorie categorie){
		
		Categorie categorieExist=categorieService.getOne(idCategorie) ;
		if(categorieExist==null) {
			ResponseEntity.notFound().build() ;
		}
		categorieExist.setNom(categorie.getNom()) ;
		categorieExist.setStatus(categorie.getStatus());
		categorieService.save(categorieExist) ;
		return ResponseEntity.ok().body(categorieExist) ;
	}
 
	
	//get categories
	
	@GetMapping(value="listCategories")
	public List<Categorie> getListCategorie(){
		
		return categorieService.findAll() ;
	}
	
	//delete categorie
	@DeleteMapping(value="/deleteCategorie/{id}")
	public ResponseEntity<Categorie> deleteCategorie(@PathVariable(value="id") Long idCategorie){
		Categorie categorieToDelete = categorieService.getOne(idCategorie) ;
		if(categorieToDelete==null) {
		return ResponseEntity.notFound().build() ;	
		}
		categorieService.deleteOne(categorieToDelete);
		return ResponseEntity.ok().body(categorieToDelete) ;
	}
	
	
	
	//get one categorie 
	@GetMapping(value="/getCategorie/{id}")

	public Categorie getOneCategorie(@PathVariable(value="id") Long idCategorie){
		Categorie categorieToReturn = categorieService.getOne(idCategorie) ;
		
		return categorieToReturn ;
		
	}
	
	@PutMapping(value="/startCategorie/{id}")
	
	public ResponseEntity<Categorie> startCategorie(@PathVariable(value="id") Long idCategorie) {
		Categorie categorie = categorieService.getOne(idCategorie) ;
		if(categorie==null) {
			ResponseEntity.notFound().build() ;
		}
		categorie.setStatus("en cours");
		categorieService.save(categorie) ;
		return ResponseEntity.ok().body(categorie) ;
	}
	
	@GetMapping(value="categorieByStatus/{status}")
	
	public List<Categorie> getCategorieByStatus(@PathVariable(value="status") String status){
		return categorieService.findByStatus(status) ;
	}
	
}
