package com.FiveSkills.BackEndV01.participantconfirme.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="participantsconfirme")
public class ParticipantConfirme {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	
	private String nom ;
	
	private String prenom ;
	
	private String cin ;
	
	private String mail ;
	
	private String telephone ;
	
	private String dateconfirmation ;
	
	private String profession ;
	
	private String lieudetravail ;
	
	private Long sessionid ;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getDateconfirmation() {
		return dateconfirmation;
	}

	public void setDateconfirmation(String dateconfirmation) {
		this.dateconfirmation = dateconfirmation;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getLieudetravail() {
		return lieudetravail;
	}

	public void setLieudetravail(String lieudetravail) {
		this.lieudetravail = lieudetravail;
	}

	public Long getSessionid() {
		return sessionid;
	}

	public void setSessionid(Long sessionid) {
		this.sessionid = sessionid;
	}
	
	
}
