package com.FiveSkills.BackEndV01.participantconfirme.controller;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.FiveSkills.BackEndV01.participantconfirme.model.ParticipantConfirme;
import com.FiveSkills.BackEndV01.participantconfirme.service.ParticipantConfirmeService;
import com.FiveSkills.BackEndV01.session.model.Session;
import com.FiveSkills.BackEndV01.session.service.SessionService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping(value="/gestionParticipantConfirme")
public class ParticipantConfirmeController {

	@Autowired 
	
	ParticipantConfirmeService participantService ;
	@Autowired
	SessionService sessionService ;
	
	//save participant
	@PostMapping(value="/addParticipantConfirme")
	public ParticipantConfirme addParticipantConfirme(@Valid @RequestBody ParticipantConfirme participant) {
		Session session = sessionService.findOne(participant.getSessionid()) ;		
		session.setNombreexistent(session.getNombreexistent()+1);
		Session sessionSaved= sessionService.save(session) ;
        LocalDate localDate = LocalDate.now();
        String  dateconfirmation= DateTimeFormatter.ofPattern("yyy-MM-dd").format(localDate);
        participant.setDateconfirmation(dateconfirmation);
        return participantService.save(participant) ;
		
		
	}
	
	//get all participants
	@GetMapping(value="/listParticipantConfirme")
	public List<ParticipantConfirme> getListParticipantConfirme(){
		return participantService.findAll() ;
	}
	
	//get participant by sessionid
	
	@GetMapping(value="/listParticipantConfirmeBySessionid/{sessionid}")
	public List<ParticipantConfirme> listParticipantConfirmeBySessionid(@PathVariable(value="sessionid") Long sessionid){
		return participantService.getBySessionid(sessionid);
	}
	
	//delete participant 
	
	@DeleteMapping(value="deleteParticipantConfirme/{id}")
	public ResponseEntity<ParticipantConfirme> deleteParticipantConfirme(@PathVariable(value="id") Long participantid){
		ParticipantConfirme participant = participantService.findOne(participantid) ;
		if(participant==null) {
			return ResponseEntity.notFound().build() ;
		}
		Session session =sessionService.findOne(participant.getSessionid()) ;
		session.setNombreexistent(session.getNombreexistent()-1);
		Session SessionUpdated=sessionService.save(session) ;
		participantService.delete(participant);
		return ResponseEntity.ok().body(participant) ;
	}
	
	
	
	
	
	
	
	
}
