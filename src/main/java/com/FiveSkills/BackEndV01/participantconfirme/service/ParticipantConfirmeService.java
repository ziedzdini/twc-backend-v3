package com.FiveSkills.BackEndV01.participantconfirme.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FiveSkills.BackEndV01.participantconfirme.model.ParticipantConfirme;
import com.FiveSkills.BackEndV01.participantconfirme.repository.ParticipantConfirmeRepository;

@Service
public class ParticipantConfirmeService {
	
	@Autowired
	ParticipantConfirmeRepository participantConfirmeRepository ;
	
	//save a participant
	
	public ParticipantConfirme save(ParticipantConfirme participant) {
		return participantConfirmeRepository.save(participant) ;
	}
	
	//get all participants
	
	public List<ParticipantConfirme> findAll(){
		return participantConfirmeRepository.findAll() ;
	}

	//get one participant
	
	public ParticipantConfirme findOne(Long participantid) {
		return participantConfirmeRepository.findById(participantid).get() ;
	}
	
	//delete participant
	
	public void delete(ParticipantConfirme participant) {
		participantConfirmeRepository.delete(participant);
	}
	
	//get participant by sessionid
	
	public List<ParticipantConfirme> getBySessionid(Long sessionid){
		return participantConfirmeRepository.findBySessionid(sessionid) ;
	}
}
