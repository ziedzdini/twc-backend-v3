package com.FiveSkills.BackEndV01.participantconfirme.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FiveSkills.BackEndV01.participantconfirme.model.ParticipantConfirme;

public interface ParticipantConfirmeRepository extends JpaRepository<ParticipantConfirme,Long> {
	
	List<ParticipantConfirme> findBySessionid(Long sessionid) ;

}
