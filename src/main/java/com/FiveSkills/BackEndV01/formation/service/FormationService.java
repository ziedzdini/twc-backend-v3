package com.FiveSkills.BackEndV01.formation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FiveSkills.BackEndV01.formation.model.Formation;
import com.FiveSkills.BackEndV01.formation.repository.FormationRepository;

@Service
public class FormationService {

	@Autowired
	FormationRepository formationRepository ;
	
	
	//save Formation 
	
	public Formation save(Formation formation) {
		return formationRepository.save(formation) ;
	}
	
	//get list Formation 
	
	public List<Formation> findAll(){
		return formationRepository.findAll() ;
	}
	
	//get one Formation 
	
	public Formation findOne(Long idFormation) {
		return formationRepository.findById(idFormation).get() ;
	}
	
	//delete Formation
	
	public void delete(Formation formation) {
		formationRepository.delete(formation);
	}
	public List<Formation> findByStatus(String status){
	 
		return formationRepository.findByStatus(status) ;
	}
}
