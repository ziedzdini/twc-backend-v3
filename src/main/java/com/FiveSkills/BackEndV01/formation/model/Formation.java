package com.FiveSkills.BackEndV01.formation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="Formations")
@EntityListeners(AuditingEntityListener.class)
public class Formation {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	
	@Column(name="nom")
	private String nom ;
	
	@Column(name="categorie")
	private String categorie ;
	
	@Column(name="formateur")
	private String formateur ;
	
	@Column(name="duree")
	private String duree ;
	
	@Column(name="lieu")
	private String lieu ;
	
	@Column(name="certification")
	private String certification ;
	
	@Column(name="creditpoint")
	private Long creditpoint ;
	
	@Column(name="prix")
	private String prix ;
	
	@Column(name="status")
	private String status ;
	
	@Column(name="description")
	private String description ;
	
	@Column(name="prerequis")
	private String prerequis ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public String getDuree() {
		return duree;
	}

	public void setDuree(String duree) {
		this.duree = duree;
	}

	public String getFormateur() {
		return formateur;
	}

	public void setFormateur(String formateur) {
		this.formateur = formateur;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getCertification() {
		return certification;
	}

	public void setCertification(String certification) {
		this.certification = certification;
	}

	public Long getCreditPoint() {
		return creditpoint;
	}

	public void setCreditPoint(Long creditPoint) {
		this.creditpoint = creditPoint;
	}

	public String getPrix() {
		return prix;
	}

	public void setPrix(String prix) {
		this.prix = prix;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrerequis() {
		return prerequis;
	}

	public void setPrerequis(String prerequis) {
		this.prerequis = prerequis;
	}
	
	
}
