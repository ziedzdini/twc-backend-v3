package com.FiveSkills.BackEndV01.formation.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import com.FiveSkills.BackEndV01.formation.model.Formation;
import com.FiveSkills.BackEndV01.formation.service.FormationService;
@CrossOrigin 
//@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping(value="/gestionFormation")
public class FormationController {

	@Autowired 
	FormationService formationService ;
	
	//***************get all Formations******************
	
	@GetMapping(value="/listFormation")
	public List<Formation> getListFormation(){
		return formationService.findAll() ;
	}
	
	//**************add Formation***********************
	
	@PostMapping(value="/addFormation")
	public Formation addFormation(@Valid @RequestBody Formation formation){
		String newPrix=formation.getPrix() ;
		formation.setPrix(newPrix+" DT");
		newPrix=formation.getDuree() ;
		formation.setDuree(newPrix+" h");
		
		return formationService.save(formation) ;
	}
	
	//***************delete Formation**************
	
	@DeleteMapping(value="/deleteFormation/{id}")
	public ResponseEntity<Formation> deleteFormation(@PathVariable("id") Long idFormation){
		Formation formation =formationService.findOne(idFormation) ;
		if(formation==null) {
			return ResponseEntity.notFound().build() ;
		}
		formationService.delete(formation);
		return ResponseEntity.ok().body(formation) ;
		
	}
	
	//******************get one Formation*****************
	
	@GetMapping(value="/getFormation/{id}")
	public ResponseEntity <Formation> getFormation(@PathVariable("id") Long idFormation) {
		Formation formation =formationService.findOne(idFormation) ;
		if(formation==null) {
			return ResponseEntity.notFound().build();
			
		}
		return ResponseEntity.ok().body(formation) ;
	}
	
	//***********************update Formation*************
	
	@PutMapping(value="/updateFormation/{id}")
	public ResponseEntity<Formation> updateFormation(@PathVariable("id") Long idFormation ,@Valid @RequestBody Formation formation){
		Formation formationExist =formationService.findOne(idFormation) ;
		if(formationExist==null) {
			return ResponseEntity.notFound().build() ;
		}
		formationExist.setNom(formation.getNom());
		formationExist.setCategorie(formation.getCategorie());
		formationExist.setCertification(formation.getCertification());
		formationExist.setCreditPoint(formation.getCreditPoint());
		formationExist.setDescription(formation.getDescription());
		formationExist.setDuree(formation.getDuree());
		formationExist.setFormateur(formation.getFormateur());
		formationExist.setLieu(formation.getLieu());
		formationExist.setPrerequis(formation.getPrerequis());
		formationExist.setPrix(formation.getPrix());
		formationExist.setStatus(formation.getStatus());
		Formation formationAfterUpdate=formationService.save(formationExist) ;
		return ResponseEntity.ok().body(formationAfterUpdate);
	}
	
	
	
	@GetMapping(value="/getFormationByStatus/{status}")
	public List<Formation> getFormationByStatus(@PathVariable("status") String status){
		return formationService.findByStatus(status);
		
	}
	
	
	@PutMapping(value="/startFormation/{id}")
	public ResponseEntity<Formation> startFormation(@PathVariable(value="id") Long idFormation) {
		Formation formation =formationService.findOne(idFormation) ;
		if(formation==null) {
			return ResponseEntity.notFound().build() ;
		}
		formation.setStatus("En Cours");
		formationService.save(formation) ;
		return ResponseEntity.ok().body(formation) ;
	}
	
	
}
