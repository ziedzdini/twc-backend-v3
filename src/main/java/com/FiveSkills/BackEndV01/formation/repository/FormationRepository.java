package com.FiveSkills.BackEndV01.formation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FiveSkills.BackEndV01.formation.model.Formation;

public interface FormationRepository extends JpaRepository<Formation,Long> {

	List<Formation> findByStatus(String status) ;
}
