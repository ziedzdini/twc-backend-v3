package com.FiveSkills.BackEndV01.objectifsDeFormation.model;

import javax.annotation.Generated;
import javax.persistence.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="objectifs")
@EntityListeners(AuditingEntityListener.class)
public class ObjectifsDeFormation {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	
	private String objectif ;
	
	private Long formationid ;

	public Long getFormationid() {
		return formationid;
	}

	public void setFormationid(Long formationid) {
		this.formationid = formationid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getObjectif() {
		return objectif;
	}

	public void setObjectif(String objectif) {
		this.objectif = objectif;
	}
	
	
}
