package com.FiveSkills.BackEndV01.objectifsDeFormation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FiveSkills.BackEndV01.objectifsDeFormation.model.ObjectifsDeFormation;

public interface ObjectifsDeFormationRepository extends JpaRepository<ObjectifsDeFormation,Long> {

	List<ObjectifsDeFormation>findByFormationid(Long formationid) ;
}
