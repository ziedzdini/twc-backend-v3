package com.FiveSkills.BackEndV01.objectifsDeFormation.controller;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.FiveSkills.BackEndV01.objectifsDeFormation.model.ObjectifsDeFormation;
import com.FiveSkills.BackEndV01.objectifsDeFormation.service.ObjectifsDeFormationService;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping(value="/gestionObjectif")
public class ObjectifsDeFormationController {

	@Autowired
	ObjectifsDeFormationService objectifsService ;
	
	//add a new objectif
	
	@PostMapping(value="/addObjectif")
	public ObjectifsDeFormation addObjectif(@Valid @RequestBody ObjectifsDeFormation objectif) {
		return objectifsService.save(objectif) ;
	}
	
	//get all objectifs
	
	@GetMapping(value="/listObjectifs")
	public List<ObjectifsDeFormation> getObjectifsList(){
		
		return objectifsService.findAll() ;
	}
	
	//get one objectif
	
	@GetMapping(value="/getOneObjectif/{id}")
	public ResponseEntity<ObjectifsDeFormation> getOneObjectif(@PathVariable(value="id") Long objectifId){
		
		ObjectifsDeFormation objectif =objectifsService.findOne(objectifId) ;
		if(objectif==null) {
		return	ResponseEntity.notFound().build() ;
		}
		
		return ResponseEntity.ok().body(objectif) ;
		
	}
	
	//delete an objectif
	
	@DeleteMapping(value="/deleteObjectif/{id}")
	public ResponseEntity<ObjectifsDeFormation> deleteOneObjectif(@PathVariable(value="id") Long objectifId){
		ObjectifsDeFormation objectif =objectifsService.findOne(objectifId) ;
		if(objectif==null) {
			return ResponseEntity.notFound().build() ;
		}
		objectifsService.delete(objectif);
		return ResponseEntity.ok().body(objectif) ;
	}
	
	//update one objectif
	
	@PutMapping(value="/updateObjectif/{id}")
	public ResponseEntity<ObjectifsDeFormation> updateObjectif(@PathVariable(value="id") Long objectifId ,@Valid @RequestBody ObjectifsDeFormation objectif){
		
		ObjectifsDeFormation objectifInDB=objectifsService.findOne(objectifId) ;
		if(objectifInDB==null) {
			return ResponseEntity.notFound().build() ;
		}
		objectifInDB.setObjectif(objectif.getObjectif()) ;
		objectifsService.save(objectifInDB) ;
		return ResponseEntity.ok().body(objectifInDB);
	}
	
	//find objectifs by formationid 
	
	@GetMapping(value="/getObjectifByFormationId/{formationid}")
	
	public List<ObjectifsDeFormation> getObjectifByFormationId(@PathVariable(value="formationid") Long formationid){
		return objectifsService.findbyFormationid(formationid) ;
	}
	
	
}

