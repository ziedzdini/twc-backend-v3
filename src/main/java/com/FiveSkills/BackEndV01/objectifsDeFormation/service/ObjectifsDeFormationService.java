package com.FiveSkills.BackEndV01.objectifsDeFormation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FiveSkills.BackEndV01.objectifsDeFormation.model.ObjectifsDeFormation;
import com.FiveSkills.BackEndV01.objectifsDeFormation.repository.ObjectifsDeFormationRepository;

@Service
public class ObjectifsDeFormationService {
	
	@Autowired
	ObjectifsDeFormationRepository objectifsRepository ;
	
	//save an objectif
	
	public ObjectifsDeFormation save(ObjectifsDeFormation objectif) {
		return objectifsRepository.save(objectif) ;
	}
	
	//get all objectifs
	
	public List<ObjectifsDeFormation> findAll() {
		return objectifsRepository.findAll() ;
	}
	
	//get one objectif
	
	public ObjectifsDeFormation findOne(Long objectifId) {
		return objectifsRepository.findById(objectifId).get() ;
	}
	
	public void delete(ObjectifsDeFormation objectif) {
		objectifsRepository.delete(objectif);
	}
	
	//get  objectifs by formationid
	
	public List<ObjectifsDeFormation> findbyFormationid(Long formationid){
		return objectifsRepository.findByFormationid(formationid) ;
	}

}
